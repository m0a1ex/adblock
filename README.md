[appasset/adblock][appasset]
============================

Links
-----

### [GitHub][appasset]

*   [ru-adlist-plus-css-fixes.ini][raw]


* * *

Links
-----

### Google Chrome

Extensions

*   [AdBlock Plus](http://bit.ly/2DA7vcK)

    <http://adblockplus.org/>

    ![icon](https://i.imgur.com/UCn2pgE.png)
    
    __Adblock Plus__ является самым популярным в мире расширением для браузеров *(более 200 миллионов скачиваний)*. Этот проект поддерживается большим сообществом, насчитывающим сотни добровольцев, что позволяет найти и заблокировать максимальное количество рекламы.
    
    ![screenshot](https://i.imgur.com/RKSDzmH.gif)
    
    Недавно сообщество __Adblock Plus__ представило идею Допустимой Рекламы. Не блокируя некоторые маленькие неподижные рекламные блоки вы поддерживаете веб-сайты, которые живут только за счет этого. В то же время, вы делаете это ненавязчиывм для вас образом. Эта функция может быть отключена в любое время. Больше информации по адресу <http://adblockplus.org/en/acceptable-ads>.


*   [uBlock Origin](http://bit.ly/2BgXFY1)

    <https://github.com/gorhill/uBlock>
    
    ![icon](https://i.imgur.com/cbcoonN.png)

    __uBlock Origin__ использует меньше оперативной памяти и меньше нагружает ЦП, при этом используя больше фильтров, чем другие популярные блокировщики.

    Иллюстрированный обзор его эффективности: <https://github.com/gorhill/uBlock/wiki/uBlock-vs.-ABP:-efficiency-compared>
  
    ![screenshot](https://i.imgur.com/aozqfcr.gif)
  
    По умолчанию следующие списки фильтров будут загружены и применены:
    
    -   EasyList;
    -   Список рекламных серверов Питера Лоу;
    -   EasyPrivacy;
    -   Вредоносные домены.
    
    Также на выбор доступны другие списки:
    
    -   Фанатский улучшенный список отслеживания;
    -   Хост-файл Дэна Поллока;
    -   Рекламные и отслеживающие сервера hpHosts;
    -   MVPS HOSTS;
    -   Spam404;
    -   и т.д.


[appasset]: https://github.com/appasset/adblock/raw/master/russian.txt
[raw]:      https://github.com/appasset/adblock/raw/master/ru-adlist-plus-css-fixes.ini
